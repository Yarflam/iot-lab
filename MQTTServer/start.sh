#!/bin/bash

rm -f ./logs/mosquitto.log 2> /dev/null
docker-compose -f ./docker-compose.yml up -d --force-recreate
docker logs -f MQTTServer
