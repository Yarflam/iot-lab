#!/usr/bin/python3
import paho.mqtt.client as mqtt
import configparser
import pyfirmata
import time

# Config file
config = configparser.ConfigParser()
config.read('config.private.ini') # Such like config.example.ini
confMqtt = config['MQTT']

# Variables
led = 10
ledMode = 0
topic = "mqttled"

# Main Function
def main ():
    # MQTT hooks
    def on_message(client, userdata, msg):
        global ledMode
        payload = msg.payload.decode('utf8')
        if msg.topic == topic:
            # Toggle ON/OFF
            if payload == 'ON': ledMode = 1
            if payload == 'OFF': ledMode = 0
            if payload == 'TGL':
                ledMode = 0 if ledMode == 1 else 1
            # Applying
            board.digital[led].write(ledMode)

    # MQTT
    mqttc = mqtt.Client()
    mqttc.on_message = on_message
    mqttc.username_pw_set(confMqtt['username'], confMqtt['key'])
    mqttc.connect(confMqtt['broker'], int(confMqtt['broker_port']), keepalive=60)
    mqttc.publish(topic, payload="Connected", qos=0, retain=False)
    mqttc.subscribe(topic)

    # Arduino
    board = pyfirmata.Arduino('/dev/ttyACM0')
    board.digital[led].write(ledMode)

    # Loop
    mqttc.loop_forever()

main()
